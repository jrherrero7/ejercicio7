FROM httpd:2.4
COPY ./html/html.html /usr/local/apache2/htdocs
COPY ./css /usr/local/apache2/htdocs/css
EXPOSE 80
